/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ahmadhattab.formcontroller;

/**
 * Controller class for login.xhtml
 * @author ahattab
 */

import com.ahmadhattab.JPA.controller.UsersJpaController;
import com.ahmadhattab.util.MessagesUtil;
import com.ahmadhattab.entities.Users;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

    
@Named
@ViewScoped
public class Login implements Serializable {
    
    @Inject
    private UsersJpaController usersJPAController;
     private String uname;
     private String password;

	// Default logger is java.util.logging
    private static final Logger LOG = Logger.getLogger("UsersJpaController.class");
    


    public String getUname() {
        LOG.info(uname);
        return uname;
    }

    public void setUname(String uname) {
        LOG.info(uname);
        this.uname = uname;
    }

    public String getPassword() {
        LOG.info(password);
        return password;
    }

    public void setPassword(String password) {
        LOG.info(password);
        this.password = password;
    }
	

	

	//validate login
    public String login(){
        
        String x;
     // Get Session object so that the status of the individual who just logged in
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        
        //log
        LOG.log(Level.INFO, "Entering login method email is {0} password is {1}", new Object[]{uname, password});
        
        // Used to contain that will be displayed on the login form after Login button is pressed
        FacesMessage message;
        boolean loggedIn = false;
        
        // Is there a client with these credentials
        Users users = usersJPAController.findUsers(uname, password);
        
        // There is a client so login was successful
        if (users != null) {
            loggedIn = true;
            message = MessagesUtil.getMessage(
                    "com.ahmadhattab.bundles.messages", "welcome", new Object[]{uname});
            message.setSeverity(FacesMessage.SEVERITY_INFO);
            x = "bookLibrary";
        } else {
            // Unsuccessful login
            loggedIn = false;
            // MessagesUtil simplifies creating localized messages
            message = MessagesUtil.getMessage(
                    "com.ahmadhattab.bundles.messages", "loginerror", new Object[]{uname});
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            x="Register";
        }
        // Store the outcome in the session object
        session.setAttribute("loggedIn", loggedIn);

        // Place the message in the context so that it will be displayed
        FacesContext.getCurrentInstance().addMessage(null, message);
        System.out.println("loggedIN status: " + loggedIn);
        
        return x;
    }
    public String singUp() {
        return "singUp";
    }
}

        
        
    


    

    

