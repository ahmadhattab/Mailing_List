/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ahmadhattab.formcontroller;

import com.ahmadhattab.entities.Users;
import com.ahmadhattab.JPA.controller.UsersJpaController;
import com.ahmadhattab.util.MessagesUtil;
import java.io.Serializable;
import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
// view rather than bean
import javax.faces.view.ViewScoped;

/**
 *
 * Controller class for register.xhtml
 *
 * @author ahattab
 */
@Named("register")
@ViewScoped
public class Register implements Serializable {

    @Inject
    private UsersJpaController usersJPAController;
    private Users users;

    /**
     * User created if it does not exist.
     *
     * @return
     */
    public Users getUsers() {
        if (users == null) {
            users = new Users();
        }
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }
    
     private boolean isValidUname() {
        boolean valid = false;
        String uname = users.getUname();
         System.out.println("uname: " + uname);
        if (uname != null) {
            if (usersJPAController.findUsersByUname(uname) == null) {
                valid = true;
                System.out.println("valid: " + valid);
            } else {
                FacesMessage message = MessagesUtil.getMessage(
                        "com.ahmadhattab.bundles.messages", "email.in.use", null);
                message.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext.getCurrentInstance().addMessage("signupForm:email", message);
            }
        }
        return valid;
    }
     
     /**
     * Perform email check and if successful then move on to index.xhtml
     *
     * @return
     * @throws Exception
     */
    public String signup() throws Exception {
        if (isValidUname()) {
            System.out.println("valid user name test");
            usersJPAController.create(users);
            return "index";
        }
        return null;
    }

}
