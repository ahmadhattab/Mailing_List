/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ahmadhattab.JPA.controller;

import com.ahmadhattab.JPA.controller.exceptions.NonexistentEntityException;
import com.ahmadhattab.JPA.controller.exceptions.RollbackFailureException;
import com.ahmadhattab.entities.Users;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;

/**
 *
 * @author ahattab
 */
@Named
@RequestScoped
public class UsersJpaController implements Serializable {


    @Resource
    private UserTransaction utx;
    
    @PersistenceContext
    private EntityManager em;
    
      public UsersJpaController() {
    }



    public void create(Users users) throws RollbackFailureException, Exception {
        System.out.println("Started Creatign user");
        //EntityManager em = null;
        try {
            utx.begin();
            em.persist(users);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } 
    }

    public void edit(Users users) throws NonexistentEntityException, RollbackFailureException, Exception {
        
        try {
            utx.begin();
            
            users = em.merge(users);
            utx.commit();
        } catch (NotSupportedException | SystemException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = users.getUid();
                if (findUsers(id) == null) {
                    throw new NonexistentEntityException("The users with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } 
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        
        try {
            utx.begin();
            
            Users users;
            try {
                users = em.getReference(Users.class, id);
                users.getUname();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The users with id " + id + " no longer exists.", enfe);
            }
            em.remove(users);
            utx.commit();
        } catch (NotSupportedException | SystemException | NonexistentEntityException | RollbackException | HeuristicMixedException | HeuristicRollbackException | SecurityException | IllegalStateException ex) {
            try {
                utx.rollback();
            } catch (IllegalStateException | SecurityException | SystemException re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } 
    }
     /**
     * Return all the records in the table
     * 
     * @return 
     */
    
    public List<Users> findUsersEntities() {
        return findUsersEntities(true, -1, -1);
    }
    
   /**
     * Return some of the records from the table. Useful for paginating.
     * 
     * @param maxResults
     * @param firstResult
     * @return 
     */

    public List<Users> findUsersEntities(int maxResults, int firstResult) {
        return findUsersEntities(false, maxResults, firstResult);
    }
 /**
     * 
     * 
     * @param all True means find all, false means find subset
     * @param maxResults Number of records to find
     * @param firstResult Record number to start returning records
     * @return 
     */
    private List<Users> findUsersEntities(boolean all, int maxResults, int firstResult) {
       
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Users.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
     /**
     * Find a record by primary key
     * 
     * @param id
     * @return 
     */
    public Users findUsers(Integer id) {
        
        try {
            return em.find(Users.class, id);
        } finally {
            em.close();
        }
    }
    
    /**
     * Find a record with the specified email and password
     *
     * @param uname
     * @param password
     * @return
     */
    
     public Users findUsers(String uname, String password) {
        TypedQuery<Users> query = em.createNamedQuery("Users.findByUnameAndPassword", Users.class);
        query.setParameter(1, uname);
        query.setParameter(2, password);
        List<Users> users = query.getResultList();
        if (!users.isEmpty()) {
            return users.get(0);
        }
        return null;
    }
     
     /**
     * Find a record with the specified email
     *
     * @param uname
     * @return
     */
    public Users findUsersByUname(String uname) {
        TypedQuery<Users> query = em.createNamedQuery("Users.findByUname", Users.class);
        query.setParameter("uname", uname);
        List<Users> users = query.getResultList();
        if (!users.isEmpty()) {
            return users.get(0);
        }
        return null;
    }
    /**
     * Return the number of records
     *
     * @return
     */
    

    public int getUsersCount() {
        
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Users> rt = cq.from(Users.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
