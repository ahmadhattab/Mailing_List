/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ahmadhattab.entities;

import java.io.InputStream;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
 
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
 
@ManagedBean
public class bookBean {
     
    private StreamedContent file1;
    private StreamedContent file2;
    private StreamedContent file3;
     
    public bookBean() {        
        InputStream stream1 = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/resources/books/AdventuresOfSherlockHolmes.zip");
        file1 = new DefaultStreamedContent(stream1, "file/zip", "downloaded_AdventuresOfSherlockHolmes.zip");
        
        InputStream stream2 = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/resources/books/AliceInWonderland.zip");
        file2 = new DefaultStreamedContent(stream2, "file/zip", "downloaded_AliceInWonderland.zip");
        
        InputStream stream3 = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/resources/books/MobyDick.zip");
        file3 = new DefaultStreamedContent(stream3, "file/zip", "downloaded_MobyDick.zip");
    }
 
    public StreamedContent getFile1() {
        return file1;
    }
    
     public StreamedContent getFile2() {
        return file2;
    }
     
      public StreamedContent getFile3() {
        return file3;
    }
}