/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  ahattab
 * Created: Mar 19, 2018
 */


DROP DATABASE IF EXISTS SUBCRIBERS;
CREATE DATABASE SUBCRIBERS;

USE SUBCRIBERS;

DROP USER IF EXISTS users@localhost;
CREATE USER users@'localhost' IDENTIFIED WITH mysql_native_password BY 'Silanis1' REQUIRE NONE;
GRANT ALL ON SUBCRIBERS.* TO users@'localhost';

-- This creates a user with access from any IP number except localhost
-- Use only if your MyQL database is on a different host from localhost
-- DROP USER IF EXISTS fish;
-- CREATE USER fish IDENTIFIED WITH mysql_native_password BY 'kfstandard' REQUIRE NONE;
-- GRANT ALL ON AQUARIUM TO fish;

-- This creates a user with access from a specific IP number
-- Preferable to '%'
-- DROP USER IF EXISTS fish@'192.168.0.194';
-- CREATE USER fish@'192.168.0.194' IDENTIFIED WITH mysql_native_password BY 'kfstandard' REQUIRE NONE;
-- GRANT ALL ON AQUARIUM TO fish@'192.168.0.194';

FLUSH PRIVILEGES;
