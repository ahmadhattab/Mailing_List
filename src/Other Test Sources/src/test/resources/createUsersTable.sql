USE SUBCRIBERS;

DROP TABLE IF EXISTS USERS;

CREATE TABLE USERS (
uid int(20) NOT NULL AUTO_INCREMENT, 
uname VARCHAR(60) NOT NULL, 
password VARCHAR(60) NOT NULL, 
firstName VARCHAR(60) NOT NULL, 
lastName VARCHAR(60) NOT NULL, 
streetAddress VARCHAR(60) NOT NULL, 
city VARCHAR(60) NOT NULL, 
province VARCHAR(60) NOT NULL, 
country VARCHAR(60) NOT NULL, 
postalCode VARCHAR(60) NOT NULL, 
  PRIMARY KEY  (uid)
) ENGINE=InnoDB;

INSERT INTO USERS (uname,password, firstName, lastName, streetAddress, city, province, country, postalCode) values
("adam","adam adam","adam","st","123","monreal","qc","canada","h3h2h1");


